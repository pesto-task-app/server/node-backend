const express = require('express');
const router = express.Router();
const taskController = require('../controllers/taskController');
const userController = require('../controllers/userController');

// Define user-related routes
router.post('/login', userController.loginUser);
router.post('/signup', userController.signupUser);

// Define task-related routes
router.get('/tasks', taskController.getAllTasks);
router.get('/task/:id', taskController.getTask);
router.post('/task', taskController.addTask);
router.put('/task', taskController.updateTask);
router.delete('/task/:id', taskController.deleteTask);
router.get('/task/distinct/status', taskController.getDistinctStatusCount);

module.exports = router;
