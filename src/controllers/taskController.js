const Task = require('../models/taskModel');

const getAllTasks = async (req, res) => {
    try {
        const tasks = await Task.find();
        res.status(200).json(tasks);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

const getTask = async (req, res) => {
    try {
        const task = await Task.findById(req.params.id);
        if (!task) {
            return res.status(404).json({ message: 'Task not found' });
        }
        res.status(200).json(task);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

const addTask = async (req, res) => {
    const task = new Task(req.body);
    try {
        const newTask = await task.save();
        res.status(200).json({
            Success: true,
            Message: "Task added successfully."
        });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
};

const updateTask = async (req, res) => {
    try {
        console.log("Updating task:", req.body);
        const taskId = req.body._id;
        const updatedTask = await Task.findOneAndUpdate({_id: taskId}, req.body, { new: true });

        if (!updatedTask) {
            return res.status(404).json({ message: "Task not found." });
        }

        res.status(200).json({
            Success: true,
            Message: "Task updated successfully."
        });
    } catch (error) {
        console.error("Error updating task:", error);
        res.status(500).json({ message: "Internal server error." });
    }
};


const deleteTask = async (req, res) => {
    try {
        await Task.findByIdAndDelete(req.params.id);
        res.status(200).json({ message: 'Task deleted successfully' });
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};


const getDistinctStatusCount = async (req, res) => {
    try {
        const statusCounts = await Task.aggregate([
            {
                $group: {
                    _id: '$status',
                    count: { $sum: 1 }
                }
            }
        ]);

        const formattedCounts = statusCounts.map(({ _id, count }) => ({
            status: _id,
            count
        }));

        res.status(200).json(formattedCounts);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

module.exports = {
    getAllTasks,
    getTask,
    addTask,
    updateTask,
    deleteTask,
    getDistinctStatusCount 
};
