const User = require('../models/userModel');

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const loginUser = async (req, res) => {
    try {
        const { username, password } = req.body;
        const user = await User.findOne({ $or: [{ email: username }, { username }] });
        if (!user) {
            return res.status(401).json({ message: 'Invalid credentials' });
        }
        const passwordMatch = await bcrypt.compare(password, user.password);
        if (!passwordMatch) {
            return res.status(401).json({ message: 'Invalid credentials' });
        }
        // Create a JWT token
        const token = jwt.sign({ userId: user._id }, 'your-secret-key', { expiresIn: '1h' });
        return res.status(200).json({ token, username });
    } catch (error) {
        console.error(error);
        return res.status(500).json({ message: 'Login failed' });
    }
};

const signupUser = async (req, res) => {
    try {
        const { email, username, password } = req.body;

        // Check if username or email already exists
        const existingUser = await User.findOne({ $or: [{ email }, { username }] });
        if (existingUser) {
            if (existingUser.email === email) {
                return res.status(400).json({ message: 'User with this email already exists' });
            } else {
                return res.status(400).json({ message: 'User with this username already exists' });
            }
        }
        const hashedPassword = await bcrypt.hash(password, 10);
        const newUser = new User({ email, username, password: hashedPassword });
        await newUser.save();
        return res.status(201).json({ message: 'User signed up successfully' });
    } catch (error) {
        console.error(error);
        return res.status(500).json({ message: 'User registration failed' });
    }
};

module.exports = {
    loginUser,
    signupUser
}