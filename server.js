const express = require("express")
const dotenv = require("dotenv")
const mongoose = require("mongoose")
const bodyParser = require('body-parser');
const cors = require('cors');
const http = require('http');

dotenv.config();

const app = express();
const server = http.createServer(app);

// Middleware
app.use(cors());
app.use(express.json());
app.use(bodyParser.json());

const userRoutes = require('./src/routes/userRoutes');

// Routes
app.use('/api/v1/user', userRoutes);

// Connect to MongoDB and start the server
const PORT = process.env.PORT || 5000;
mongoose.set("strictQuery", false)
mongoose.connect(process.env.MONGODB_URL, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        console.log('Connected to MongoDB')
        server.listen(PORT, () => {
            console.log(`Server is running on port ${PORT}`);
        });
    })
    .catch(err => console.error('MongoDB connection error:', err));
