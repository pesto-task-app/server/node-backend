## Installation
git clone https://github.com/your-username/your-repository.git
cd your-repository
npm install (node version v20.11.0)

## Configuration
Configure environment variables:
    PORT=3001
    DEBUG=true
    MONGODB_URL=mongodb+srv://admin:admin@mycluster.lag3igq.mongodb.net/
    DB_USERNAME=myuser
    DB_PASSWORD=mypassword

## Usage
npm start

The server should now be running at http://localhost:3001 (or the port specified in your .env file).